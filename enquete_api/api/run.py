import sys
sys.path.append('/usr/src/app/api')
import falcon
from main.ressource.agent import Agents, Agent
from main.ressource.survey import Surveys
from main.ressource.auth import Auth
from main.ressource.user_activity import UsersActivities
from main.util.helpers import RequireJSON, JSONTranslator, CORSComponent, AuthMiddleware


# main api app
app = falcon.API()


#set up middle war
app = falcon.API(middleware=[
    CORSComponent(),
    RequireJSON(),
    JSONTranslator()
])

#declare all request here intantiator
agents = Agents()
agent = Agent()
surveys = Surveys()
auth = Auth()
users_activities = UsersActivities()

# handle all requests to the '/' URL
base_rightq_route = '/enquetemystere/api'

app.add_route(base_rightq_route+'/agents', agents)
app.add_route(base_rightq_route+'/agent/{agent_id}', agent)
app.add_route(base_rightq_route+'/surveys', surveys)
app.add_route(base_rightq_route+'/auth', auth)
app.add_route(base_rightq_route+'/activities', users_activities)
#app.add_route(base_rightq_route+'/service/{service_id}', service)