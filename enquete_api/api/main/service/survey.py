import sys
sys.path.append("/usr/src/app/api")
import uuid
import time
import datetime
import json
import hashlib
import os
from main.util.helpers import getlogger,connect_to_db_mongo,response,redis_get_value_hmap, valid_email, encrypt_pass, add_to_redis, redis_key_exist_in_hmap, add_to_redis_in_hmap, add_to_redis, redis_get_value, redis_del_key, notifiy_errors_by_mail
from main.util import constant as cst
#from main.socket.rightq_socket import emit_to
#from main.service.common_utils import  check_links, remove_links


log = getlogger(cst.LOGGER["SURVEY_LOGIC"], cst.LOG_FILES["SURVEY_LOGIC_LOG"])



#counter logic to store survey
def store_survey(data):
    log.info("Running in survey creation logic")
    log.info(data)
    survey_data = data["survey"]
    
    try:

        #
        start_time = time.time()

        log.info("check user sesion in redis")
        agent_email = redis_get_value(str(survey_data["agent"]["id"]))
        if agent_email is None:
            error = {
            "code": cst.ERROR_CODE["AGENT_NOT_FOUND"],
            "params": "store survey service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
    
        token = redis_get_value(agent_email)
        if token is None or token != data["session"]:
            error = {
            "code": cst.ERROR_CODE["AGENT_SESSION_NOT_VALID"],
            "params": "store survey service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)

        log.info("check survey content")

        if not isinstance(survey_data["surveys"], list):
            error = {
                "code": cst.ERROR_CODE["INVALID_FORMAT"],
                "params": "store survey service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
        
        log.info("Format data to store")
        #format data to store in mongo
        survey_to_store_arrays = []
        for survey in survey_data["surveys"]:           
            to_store = survey
            to_store["id"] = uuid.uuid1()
            to_store["operation_date"] = datetime.datetime.now()
            to_store["created_date"] = datetime.datetime.fromtimestamp(survey["created_date"])
            to_store["updated_date"] = datetime.datetime.fromtimestamp(survey["updated_date"])
            to_store["started_date"] = datetime.datetime.fromtimestamp(survey["started_date"])
            to_store["ended_date"] = datetime.datetime.fromtimestamp(survey["ended_date"])
            if "status" not in survey:
                to_store["status"] = cst.STATUS_SURVEY[0]
            else:
                 to_store["status"] = cst.STATUS_SURVEY[int(survey["status"])]
            if "agent" not in survey and "agent" in survey_data:
                to_store["agent"] = survey_data["agent"]

            survey_to_store_arrays.append(to_store)

        log.info(survey_to_store_arrays)

        log.info("connect to db")
        col = connect_to_db_mongo(cst.SURVEY_COLLECTION_SYNCED)
        
        if survey_to_store_arrays[0]["status"] == "VALID":
            col = connect_to_db_mongo(cst.SURVEY_COLLECTION_VALID)
        elif survey_to_store_arrays[0]["status"] == "REJECTED":
            col = connect_to_db_mongo(cst.SURVEY_COLLECTION_REJECTED)
        if col[0] == 1 and col[1]:
            data = col[1].insert_many(survey_to_store_arrays)
        log.info("finished insertion")
        log.info("Total time: %f", time.time() - start_time)       
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], None, None)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])


#agent logic to get all
def get_all_surveys(data):
    log.info("Running in surveys get logic")
    survey_data = data["survey"]
    response_array = []
    response_ = []

    log.info(survey_data)
    try:

        log.info("check user sesion in redis")
        agent_email = redis_get_value(str(survey_data["agent"]))
        if agent_email is None:
            error = {
            "code": cst.ERROR_CODE["AGENT_NOT_FOUND"],
            "params": "store survey service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
    
        token = redis_get_value(agent_email)
        if token is None or token != data["session"]:
            error = {
            "code": cst.ERROR_CODE["AGENT_SESSION_NOT_VALID"],
            "params": "store survey service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
    
        log.info("Gettings surveys ....")
        start_time = time.time()
        log.info(survey_data["status"])
        col = connect_to_db_mongo(cst.SURVEY_COLLECTION_SYNCED)
        if survey_data["status"] is None:
            col = connect_to_db_mongo(cst.SURVEY_COLLECTION_SYNCED)
        else:
            if  int(survey_data["status"]) == 1:
                col = connect_to_db_mongo(cst.SURVEY_COLLECTION_VALID)
            elif int(survey_data["status"]) == 3:
                col = connect_to_db_mongo(cst.SURVEY_COLLECTION_REJECTED)
            
        
        log.info("connect to db")
        log.info(col)
      
        if col[0] == 1 and col[1]:
            if survey_data["agent"] is None:
                response_ = col[1].find({}, {"_id":0})
            else:
                response_ = col[1].find({"agent.id":survey_data["agent"] }, {"_id":0})
            if response_:
                for survey in response_:
                    survey["id"] = str(survey["id"])
                    survey["operation_date"] = survey["operation_date"].strftime('%d-%m-%Y %H:%M:%S')
                    #survey["created_date"] = survey["created_date"].strftime('%d-%m-%Y %H:%M:%S')
                    #survey["updated_date"] = survey["updated_date"].strftime('%d-%m-%Y %H:%M:%S')
                    #survey["started_date"] = survey["started_date"].strftime('%d-%m-%Y %H:%M:%S')
                    #survey["ended_date"] = survey["ended_date"].strftime('%d-%m-%Y %H:%M:%S')
                    survey["created_date"] = int(time.mktime(survey["created_date"].timetuple()))
                    survey["updated_date"] = int(time.mktime(survey["updated_date"].timetuple()))
                    survey["started_date"] = int(time.mktime(survey["started_date"].timetuple()))
                    survey["ended_date"] = int(time.mktime(survey["ended_date"].timetuple()))
                    response_array.append(survey)
        log.info("Total time: %f", time.time() - start_time)

        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error = None, data = response_array)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Get all agents.", "There is this error: %s" % e, agent_data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])

