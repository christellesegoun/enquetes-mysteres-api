import sys
sys.path.append("/usr/src/app/api")
import uuid
import time
import datetime
import json
import hashlib
import os
from main.util.helpers import getlogger,connect_to_db_mongo,response,redis_get_value_hmap, valid_email, encrypt_pass, add_to_redis, redis_key_exist_in_hmap, add_to_redis_in_hmap, add_to_redis, redis_get_value, redis_del_key, notifiy_errors_by_mail
from main.util import constant as cst
#from main.socket.rightq_socket import emit_to
#from main.service.common_utils import  check_links, remove_links


log = getlogger(cst.LOGGER["USER_ACTIVITY_LOGIC"], cst.LOG_FILES["USER_ACTIVITY_LOGIC_LOG"])




def store_activities(data):
    log.info("Running in store_activities  logic")
    log.info(data)
    logs = data["logs"]
   
     
    try:

        #
        start_time = time.time()

       
        log.info("Format data to store")
        #format data to store in mongo
        logs_to_store_arrays = []
        for onelog in logs:    
            to_store = onelog       
            to_store["id"] = uuid.uuid1()
            to_store["created_date"] = datetime.datetime.now()
            to_store["type"] = onelog["type"]
            to_store["tag"] =  onelog["tag"]
            to_store["message"] =  onelog["message"]
            to_store["agent"] =  onelog["agent"]

            logs_to_store_arrays.append(to_store)

        log.info(logs_to_store_arrays)

        log.info("connect to db")
        col = connect_to_db_mongo(cst.ACTIVITY_COLLECTION)
        if col[0] == 1 and col[1]:
            result = col[1].insert_many(logs_to_store_arrays)
        log.info("finished insertion")
        log.info("Total time: %f", time.time() - start_time)       
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], None, None)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Store user activities an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])
 

