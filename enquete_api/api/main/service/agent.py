import sys
sys.path.append("/usr/src/app/api")
import uuid
import time
import datetime
import json
import hashlib
import os
from main.util.helpers import getlogger,connect_to_db_mongo,response,redis_get_value_hmap, valid_email, encrypt_pass, add_to_redis, redis_key_exist_in_hmap, add_to_redis_in_hmap, add_to_redis, redis_get_value, redis_del_key, notifiy_errors_by_mail
from main.util import constant as cst
#from main.socket.rightq_socket import emit_to
#from main.service.common_utils import  check_links, remove_links


log = getlogger(cst.LOGGER["AGENT_LOGIC"], cst.LOG_FILES["AGENT_LOGIC_LOG"])



#counter logic to create new counter
def create_agent(data):
    log.info("Running in agent creation logic")
    log.info(data)
    agent_data = data["agent"]
   
    try:
        start_time = time.time()
        log.info("check email valid")
        if not valid_email(agent_data["email"]):
            error = {
                "code": cst.ERROR_CODE["INVALID_EMAIL"],
                "params": "creation agent service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)

        log.info("check email exist")
        if redis_key_exist_in_hmap(cst.HMAP["agent_pass"],agent_data["email"]):
            error = {
                "code": cst.ERROR_CODE["EMAIL_EXIST"],
                "params": "creation agent service"
            }
            log.info(agent_data["email"]+" already exist")
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
        
        log.info("email not exist")

        log.info("encrypt password")
        encrypt = encrypt_pass(agent_data["password"])
        obj = {
            "KEY": agent_data["email"] ,
            "VALUE": str(encrypt)
        }
        log.info("add in redis")
        add_to_redis_in_hmap(cst.HMAP["agent_pass"],obj)
        id = uuid.uuid1()
        add_to_redis(str(id), agent_data["email"])

        #format data to store in mongo
        to_store = {
            "id": id,
            "firstname": agent_data["firstname"],
            "lastname": agent_data["lastname"],
            "email": agent_data["email"],
            "password": str(encrypt),
            "status": 1,
            "roles" : agent_data["roles"],
            "created_date": datetime.datetime.now(),
            "last_updated_date": datetime.datetime.now(),
            "user": agent_data["user"]
        }
        
        log.info("connect to db")
        col = connect_to_db_mongo(cst.AGENT_COLLECTION)
        if col[0] == 1 and col[1]:
            data = col[1].insert(to_store)
        log.info("Total time: %f", time.time() - start_time)

        to_ret = {
            "id": str(id),
            "firstname": agent_data["firstname"],
            "lastname": agent_data["lastname"],
            "email": agent_data["email"],
            "password": str(encrypt),
            "status": 1,
            "roles" : agent_data["roles"],
            "created_date": datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S'),
            "last_updated_date": datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S'),
            "user": agent_data["user"]
        }
        
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error = None, data=to_ret)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])


#agent logic to get all
def get_all_agents(agent_data):
    log.info("Running in agents get logic")
    log.info(agent_data)
    agent_result = []
   
    try:
        log.info("Gettings agents ....")
        start_time = time.time()
        log.info("connect to db")
        col = connect_to_db_mongo(cst.AGENT_COLLECTION)
        log.info(col)
        if col[0] == 1 and col[1]:
            data = col[1].find({}, {"_id":0})
            for agent in data:
                if agent["status"] == 0:
                    agent["status"] = "Inactive"
                else:
                    agent["status"] = "Active"
                agent["id"]= str(agent["id"])

                agent["created_date"] = agent["created_date"].strftime('%d-%m-%Y %H:%M:%S')
                agent["last_updated_date"] = agent["last_updated_date"].strftime('%d-%m-%Y %H:%M:%S')
                agent_result.append(agent)

        log.info("Total time: %f", time.time() - start_time)

        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error = None, data = agent_result)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Get all agents.", "There is this error: %s" % e, agent_data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])


def login(data):
    try:
        log.info(data)
        agent_data = data["agent"]
        error = None

        #check email valid
        if not valid_email(agent_data["email"]):
            error = {
                "code": cst.ERROR_CODE["INVALID_EMAIL"],
                "params": "service login",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)

        #check email exist
        log.info("check email exist")
        if not redis_key_exist_in_hmap(cst.HMAP["agent_pass"],agent_data["email"]):
            error = {
                "code": cst.ERROR_CODE["EMAIL_NOT_FOUND"],
                "params": "login service"
            }
            log.info(agent_data["email"]+" not found")
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
        
         #check password valid
        log.info("check password valid")
        agent_info = redis_get_value_hmap(cst.HMAP["agent_pass"], agent_data["email"])
        encrypt_new_pass = encrypt_pass(agent_data["password"])
        if encrypt_new_pass != agent_info:
            error = {
                "code": cst.ERROR_CODE["EMAIL_NOT_FOUND"],
                "params": "login service"
            }
            log.info(agent_data["email"]+" not found")
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)

        #check or generate token
        token = redis_get_value(str(agent_data["email"]))
        new_token = uuid.uuid1()
        log.info("generate token")           
        
        if token :
            redis_del_key(agent_data["email"])
            error = {
                "code": cst.ERROR_CODE["TOKEN_EXIST"],
                "params": "login service"
            }

        add_to_redis(agent_data["email"], new_token, 86400)
           
        
            
        #get agent info in db
        log.info("connect to db")
        col = connect_to_db_mongo(cst.AGENT_COLLECTION)
        log.info(col)
        if col[0] == 1 and col[1]:
            agent = col[1].find_one({"email":agent_data["email"]}, {"_id":0, "password":0})
            if agent["status"] == 0:
                agent["status"] = "Inactive"
                error = {
                    "code": cst.ERROR_CODE["AGENT_NOT_ACTIVE"],
                    "params": "login service"
                }
                return response(cst.HTTP_STATUS['HTTP_400'], cst.ERROR_TITLES['HTTP_400'], error=error)
            else:
                agent["status"] = "Active"
                agent["id"]= str(agent["id"])
                agent["created_date"] = agent["created_date"].strftime('%d-%m-%Y %H:%M:%S')
                agent["last_updated_date"] = agent["last_updated_date"].strftime('%d-%m-%Y %H:%M:%S')
                agent["token"] = str(new_token)
                log.info(agent)
                return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error=error, data = agent)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])

def checksessionorlogout(data):
    log.info("in checksessionorlogout")
    log.info(data)
    if data["agent"]["token"] is None:
        return logout(data)
    else:
        return checksession(data)


def logout(data):
    try:
        log.info(data)
        agent_data = data["agent"]
        email = redis_get_value(str(agent_data["id"]))
      
        #check email exist
        log.info("check email exist")
        if email is None:
            error = {
                "code": cst.ERROR_CODE["EMAIL_NOT_FOUND"],
                "params": "logout service"
            }
            log.info(agent_data["email"]+" not found")
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
        
        #delete token
        redis_del_key(email)
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error=None)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])


def checksession(data):
    try:
        log.info(data)
        agent_data = data["agent"]
        email = redis_get_value(str(agent_data["id"]))
      
        #check email exist
        log.info("check email exist")
        if email is None:
            error = {
                "code": cst.ERROR_CODE["EMAIL_NOT_FOUND"],
                "params": "logout service"
            }
            log.info(agent_data["email"]+" not found")
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
        
        token = redis_get_value(email)
        log.info(token)
        if token is None or token != agent_data["token"]:
            error = {
            "code": cst.ERROR_CODE["AGENT_SESSION_NOT_VALID"],
            "params": "check  session service",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
    
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error=None)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])



def update_agent(data):
    log.info("Running in agent update logic")
    
    log.info(data)
    agent_data = data["agent"]
    #return  response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error = None, data=None)
    try:
        start_time = time.time()

        agent_email = redis_get_value(str(agent_data["id"]))
        if agent_email is None:
            error = {
             "code": cst.ERROR_CODE["AGENT_NOT_FOUND"],
             "params": "update agent",
            }
            log.info(error)
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)

        
        #format data to store in mongo
  
        
        to_store = {
             "$set":{
                "firstname": agent_data["firstname"],
                "lastname": agent_data["lastname"],
                "roles" : agent_data["roles"],            
                "last_updated_date": datetime.datetime.now(),
                "user": agent_data["user"]
             }
        }
        #check if password is empty and change it in redis if it isn't empty
        if  agent_data["password"] != "":
            log.info("encrypt password")
            encrypt = encrypt_pass(agent_data["password"])
            to_store["$set"].update(password = str(encrypt))
            obj = {
                "KEY": agent_data["email"] ,
                "VALUE": str(encrypt)
            }
            log.info("add in redis")
            add_to_redis_in_hmap(cst.HMAP["agent_pass"],obj)



        log.info("connect to db")
        col = connect_to_db_mongo(cst.AGENT_COLLECTION)
        if col[0] == 1 and col[1]:
            data = col[1].update_one({"id": uuid.UUID(agent_data["id"])},to_store)
        log.info("Total time: %f", time.time() - start_time)

        to_ret = {
            "id": str(agent_data["id"]),
            "firstname": agent_data["firstname"],
            "lastname": agent_data["lastname"],
            "email": agent_data["email"],
            "roles" : agent_data["roles"],
            "created_date": datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S'),
            "last_updated_date": datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S'),
            "user": agent_data["user"]
        }
        
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error = None, data=to_ret)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])

def deactivate_agent(data):
    try:
        log.info(data)
        start_time = time.time()
        agent_data = data["agent"]
        email = redis_get_value(str(agent_data["id"]))
      
        #check email exist
        log.info("check email exist")
        if email is None:
            error = {
                "code": cst.ERROR_CODE["EMAIL_NOT_FOUND"],
                "params": "logout service"
            }
            log.info(agent_data["email"]+" not found")
            return  response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
        
        #delete token
        redis_del_key(email)
        redis_get_value(agent_data["id"])

        to_store = {
            "$set":{
                "status": 0,          
                "last_updated_date": datetime.datetime.now(),
                "user": agent_data["user"]
            }
        }
        col = connect_to_db_mongo(cst.AGENT_COLLECTION)
        if col[0] == 1 and col[1]:
            data = col[1].update_one({"id": uuid.UUID(agent_data["id"])},to_store)
        log.info("Total time: %f", time.time() - start_time)
        return response(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], error=None)
    except Exception as e:
        log.exception("Operation failed: %s" % e)
        notifiy_errors_by_mail("Add an  agent.", "There is this error: %s" % e, data)
        return  response(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'])









#counter logic to get one counter
def get_one_counter(counter_data):
    log.info("Running in counter get logic")
    log.info(counter_data)
    #For test. | This must be remove after account Integration
    #counter_data["db"] = cst.CASSANDRA_TEST_KEY_SPACE

    try:
        log.info("Getting database session")
        #TO DO | Add client info from token
        session = get_keyspace_session(keyspace=counter_data["db"])
        session.row_factory = dict_factory
        log.info("Gotten database connection for operation")

        #TO DO | Normaly must consider user timezone and GET User id from account Token
        log.info("Gettings counter ....")
        start_time = time.time()

        cql_query = "SELECT * FROM counter WHERE  id={}".format(uuid.UUID(counter_data["counter"]["id"]))
        counter_future = session.execute_async(cql_query)
        #counter_future.add_callback(success_get_all_counters, time.time(), sio_channel=counter_data["sio_channel"])
        #counter_future.add_errback(error_get_all_counters, time.time(), sio_channel=counter_data["sio_channel"])
        #counter_future = counter_future.result()

        counter_result = []

        try:
            counter_res_tmp = counter_future.result()
            log.info("Total time: %f", time.time() - start_time)

            for r in counter_res_tmp:

                log.info(r)
                r["created_at"] = str(r["created_at"])
                r["updated_at"] = str(r["updated_at"])
                r["id"] = str(r["id"])
                r["group_name"] = ""
                r["group_description"] = ""
                if r["group"] != "":
                    cql_query = "SELECT id, name, description FROM group_counter WHERE id={}".format(r["group"])
                    future = session.execute_async(cql_query)
                    r["group"] = ""
                    try:
                        res_ser = future.result()
                        for gr in list(res_ser):
                            log.info(gr)
                            r["group"] = str(gr["id"])
                            r["group_name"] = gr["name"]
                            r["group_description"] = gr["description"]
                            break
                    except Exception as e:
                        log.exception("Operation failed: %s" % e)
                        emit_to(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'], {}, counter_data["sio_channel"])

                counter_result = r
                break

            emit_to(cst.HTTP_STATUS['HTTP_200'], cst.ERROR_TITLES['HTTP_200'], counter_result,
                    counter_data["sio_channel"])

        except Exception as e:
            log.exception("Operation failed: %s" % e)
            notifiy_errors("Get one counter. Exception", "There is this error: %s" % e, counter_data)
            emit_to(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'], {}, counter_data["sio_channel"])


    except Exception as e:
        log.info("Unhandled exception occurs %s" % e)
        notifiy_errors("Get one counter. Main Exception", "There is this error: %s" % e, counter_data)
        emit_to(cst.HTTP_STATUS['HTTP_500'], cst.ERROR_TITLES['HTTP_500'], {}, counter_data["sio_channel"])






