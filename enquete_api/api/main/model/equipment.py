from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class Equipment(Model):
    __table_name__ = 'equipment'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    code = columns.Text(index=True)
    description = columns.Text()
    group = columns.Text(index=True)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()
