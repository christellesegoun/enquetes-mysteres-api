from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model


class Counter(Model):
    __table_name__ = 'counter'
    id = columns.UUID(primary_key=True)
    code = columns.Text(required=True, index=True)
    name = columns.Text(required=True, index=True)
    group = columns.Text(index=True)
    description = columns.Text()
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()