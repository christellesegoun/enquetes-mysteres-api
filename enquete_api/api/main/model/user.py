from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class User(Model):
    __table_name__ = 'user'
    id = columns.UUID(primary_key=True)
    admin_id = columns.Text()
    firstname = columns.Text(required=True, index=True)
    lastname = columns.Text(required=True, index=True)
    email = columns.Text(required=True, index=True)
    phone = columns.Text(index=True)
    profile = columns.Text(index=True)
    subcription_type = columns.UUID(index=True)
    created_at = columns.DateTime()
    updated_at = columns.DateTime()
    queues = columns.Map(key_type=columns.Text(), value_type=columns.Text())
    image = columns.Text()