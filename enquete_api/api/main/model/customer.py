from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class Customer(Model):
    __table_name__ = 'customer'
    id = columns.UUID(primary_key=True)
    firstname = columns.Text(required=True, index=True)
    lastname = columns.Text(required=True, index=True)
    email = columns.Text(required=True, index=True)
    phone = columns.Text(required=True, index=True)
    address = columns.Text()
    group = columns.Text(index=True)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()
