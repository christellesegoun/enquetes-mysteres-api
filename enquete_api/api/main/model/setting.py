from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class Setting(Model):
    __table_name__ = 'setting'
    id = columns.UUID(primary_key=True)
    active = columns.Integer(required = True)
    availability = columns.Integer(required = True)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()
