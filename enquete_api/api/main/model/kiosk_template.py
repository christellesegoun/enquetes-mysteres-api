from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class KioskTemplate(Model):
    __table_name__ = 'kiosk_template'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    builder_json = columns.Map(columns.Text, columns.Text)
    description = columns.Text(index=True)
    category = columns.Integer(required=True)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()