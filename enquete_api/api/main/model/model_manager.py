import sys
sys.path.append("/usr/src/app/api")
import uuid
import datetime
from cassandra.cqlengine import connection
from cassandra.cluster import Cluster
from cassandra.cqlengine.management import sync_table, create_keyspace_simple
from cassandra.auth import PlainTextAuthProvider

from main.model.counter import Counter
from main.model.service import Service
from main.model.customer import Customer
from main.model.equipment import Equipment
from main.model.group_counter import GroupCounter
from main.model.group_service import GroupService
from main.model.group_customer import GroupCustomer
from main.model.group_equipment import GroupEquipment
from main.model.group_queue import GroupQueue
from main.model.tag import Tag
from main.model.license import License
from main.model.rq_queue import Queue
from main.model.user import User
from main.model.subscription_type import SubscriptionType
from main.model.sub_service import SubService
from main.model.kiosk_template import KioskTemplate
from main.model.parcours import Parcours
from main.model.setting import Setting
from main.model.ticket import Ticket
from main.util import constant as cst
from main.util.helpers import get_keyspace_session






# table sync function for creating and synchronizing
def create_and_synchronize_table(connection=None, keyspace=None):
    print("Sync table started")

    if connection is None and keyspace is None:
        sync_table(Service)
        print("Service sync")
        sync_table(Counter)
        print("Counter sync")
        sync_table(Customer)
        print("Customer sync")
        sync_table(Equipment)
        print("Equipment sync")
        sync_table(GroupCounter)
        print("GroupCounter sync")
        sync_table(GroupService)
        print("GroupService sync")
        sync_table(GroupCustomer)
        print("GroupCustomer sync")
        sync_table(GroupEquipment)
        print("GroupEquipment sync")
        sync_table(GroupQueue)
        print("GroupQueue sync")
        sync_table(Tag)
        print("Tab sync")
        sync_table(Queue)
        print("Queue sync")
        sync_table(SubscriptionType)
        print("Subscription sync")
        sync_table(User)
        print("User sync")
        sync_table(SubService)
        print("SubService sync")
        sync_table(KioskTemplate)
        print("KioskTemplate sync")
        sync_table(Parcours)
        print("Parcours sync")
        sync_table(Setting)
        print("Setting sync")
        sync_table(Ticket)
        print("Ticket sync")
        print("Sync table ended")
    else:
        sync_table(Service)
        print("Service sync")
        sync_table(Counter, keyspaces=[keyspace])
        print("Counter sync")
        sync_table(Customer, keyspaces=[keyspace])
        print("Customer sync")
        sync_table(Equipment, keyspaces=[keyspace])
        print("Equipment sync")
        sync_table(GroupCounter, keyspaces=[keyspace])
        print("GroupCounter sync")
        sync_table(GroupService, keyspaces=[keyspace])
        print("GroupService sync")
        sync_table(GroupCustomer, keyspaces=[keyspace])
        print("GroupCustomer sync")
        sync_table(GroupEquipment, keyspaces=[keyspace])
        print("GroupEquipment sync")
        sync_table(GroupQueue, keyspaces=[keyspace])
        print("GroupQueue sync")
        sync_table(Tag, keyspaces=[keyspace])
        print("Tab sync")
        sync_table(Queue, keyspaces=[keyspace])
        print("Queue sync")
        sync_table(SubscriptionType, keyspaces=[keyspace])
        print("Subscription sync")
        sync_table(User, keyspaces=[keyspace])
        print("User sync")
        sync_table(SubService, keyspaces=[keyspace])
        print("SubService sync")
        sync_table(KioskTemplate, keyspaces=[keyspace])
        print("KioskTemplate sync")
        sync_table(Parcours, keyspaces=[keyspace])
        print("Parcours sync")
        sync_table(Setting, keyspaces=[keyspace])
        print("Setting sync")
        sync_table(Ticket, keyspaces=[keyspace])
        print("Ticket sync")
        print("Sync table ended")

    return True


def create_subscription_type(subscription_data, session):
    print(subscription_data)
    query = "INSERT INTO subscription_type(id , name, description, created_date) VALUES(%(id)s, %(name)s, %(description)s, %(created_date)s)";
    session.execute(query,subscription_data )
    return  None

def create_users(users_data, session):
    print(users_data)
    query = "INSERT INTO user(id , admin_id, email, phone, subcription_type, created_at, updated_at) VALUES(%(id)s, %(admin_id)s, %(email)s, %(phone)s, %(subcription_type)s, %(created_at)s, %(updated_at)s)";
    session.execute(query, users_data)
    return None

def create_kiosk_template(template_data, session):
    print(template_data)
    query = "INSERT INTO kiosk_template(id , name, category) VALUES(%(id)s, %(name)s,  %(category)s)";
    session.execute(query, template_data)
    return None

def create_keyspace(cluster, name, network=True):
    session = cluster.connect()
    if network:
        session.execute("CREATE KEYSPACE IF NOT EXISTS {} WITH REPLICATION = {};".format(name, { 'class' : 'SimpleStrategy', 'replication_factor' : 3 }))
        print("Done creating keyspace")
    return True

def create_first_setting(setting_data, session):
    query = "INSERT INTO setting(id , active, availability, created_at, created_by, updated_at, updated_by) VALUES(%(id)s, %(active)s,  %(availability)s, %(created_at)s, %(created_by)s, %(updated_at)s, %(updated_by)s)";
    session.execute(query, setting_data)
    return None


def initialise_default_table(session):
    print("Init tables")

    #------- init subscription type -----#
    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "Lite",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)
    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "Standard",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)
    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "Professional",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)
    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "Enterprise",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)
    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "QLib Free",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)

    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "QLib",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)
    create_subscription_type({
        "id": uuid.uuid1(),
        "name": "Pro",
        "description": "",
        "created_date": datetime.datetime.utcnow()
    }, session)


    #--------- Init kiosk template ---------#

    create_kiosk_template({
        "id": uuid.uuid1(),
        "name": "Parcours 1",
        "category": 0,
        "created_date": datetime.datetime.utcnow()
    }, session)

    create_kiosk_template({
        "id": uuid.uuid1(),
        "name": "Parcours 2",
        "category": 1,
        "created_date": datetime.datetime.utcnow()
    }, session)

    create_kiosk_template({
        "id": uuid.uuid1(),
        "name": "Parcours 3",
        "category": 0,
        "created_date": datetime.datetime.utcnow()
    }, session)

    create_kiosk_template({
        "id": uuid.uuid1(),
        "name": "Parcours 4",
        "category": 0,
        "created_date": datetime.datetime.utcnow()
    }, session)


    print("Done initializing tables content")



if __name__ == "__main__":
    print("Starting connection setup connection")
    #create keyspaces
    auth_provider = PlainTextAuthProvider(username=cst.CASSANDRA_CLUSTER_USER, password=cst.CASSANDRA_CLUSTER_PASSWORD)
    cluster = Cluster(cst.CASSANDRA_CLUSTER_HOST, auth_provider=auth_provider, port=cst.CASSANDRA_CLUSTER_PORT)
    #create_keyspace(cluster, cst.CASSANDRA_TEST_KEY_SPACE)
    #create_keyspace(cluster, "mytest")
    print("There is done")
    session = cluster.connect(keyspace=cst.CASSANDRA_TEST_KEY_SPACE)
    #session = cluster.connect(keyspace=cst.CASSANDRA_MAIN_DB)
    connection.set_session(session)
    #create_and_synchronize_table()
    #sync_table(License)
    #create_and_synchronize_table()
    res = session.execute("SELECT * FROM system.schema_keyspaces")
    for r in list(res):
        print(r)
    # create_first_setting({
    #     "id": uuid.uuid1(),
    #     "active":0,
    #     "availability":0,
    #     "created_at": datetime.datetime.utcnow(),
    #     "updated_at": datetime.datetime.utcnow(),
    #     "created_by": cst.DEFAULT_TEST_USER_ACCOUNT_ID,
    #     "updated_by": cst.DEFAULT_TEST_USER_ACCOUNT_ID,
    # }, session)

