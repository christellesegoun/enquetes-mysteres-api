from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class Parcours(Model):
    __table_name__ = 'parcours'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    code = columns.Text(required=True, index=True)
    kiosktemplate= columns.UUID()
    queue = columns.UUID()
    description = columns.Text()
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()