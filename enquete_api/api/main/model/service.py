from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class Service(Model):
    __table_name__ = 'service'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    code = columns.Text(required=True, index=True)
    image = columns.Text()
    priority = columns.SmallInt(required=True, index=True)
    color = columns.Text()
    group = columns.Text(index=True)
    name_tr = columns.Map(key_type=columns.Text, value_type=columns.Text)
    queue = columns.UUID()
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()



