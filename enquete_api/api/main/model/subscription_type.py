from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class SubscriptionType(Model):
    __table_name__ = 'subscription_type'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    description = columns.Text()
    created_date = columns.DateTime()

