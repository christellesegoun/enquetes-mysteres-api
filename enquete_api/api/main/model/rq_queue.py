from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class Queue(Model):
    __table_name__ = 'queue'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    description = columns.Text()
    group = columns.Text(index=True)
    subscription_type = columns.UUID(index=True, required=True)
    active = columns.Boolean(required=True)
    services = columns.Map(columns.Text, columns.Text)
    users = columns.Map(columns.Text, columns.Text)
    counters = columns.Map(columns.Text, columns.Text)
    services_hours = columns.Map(columns.Text, columns.Text)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()
