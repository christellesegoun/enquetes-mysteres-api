from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



class SubService(Model):
    __table_name__ = 'sub_service'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    image = columns.Text()
    color = columns.Text()
    name_tr = columns.Map(key_type=columns.Text(), value_type=columns.Text())
    service = columns.Map(key_type=columns.Text(), value_type=columns.Text(), index=True)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()



