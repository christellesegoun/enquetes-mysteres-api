from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model


class License(Model):
    __table_name__ = 'license'
    license = columns.UUID(primary_key=True)
    subscription = columns.Text(index=True)
    created_at = columns.DateTime()
    updated_at = columns.DateTime()
    duration = columns.Integer()
    validity = columns.Integer()
    activation = columns.DateTime()
    expiration = columns.DateTime()
