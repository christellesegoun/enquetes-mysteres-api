from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model


class Tag(Model):
    __table_name__ = 'tag'
    id = columns.UUID(primary_key=True)
    name = columns.Text(required=True, index=True)
    created_at = columns.DateTime()
    created_by = columns.Text()
    updated_at = columns.DateTime()
    updated_by = columns.Text()