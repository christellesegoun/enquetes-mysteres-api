import sys

sys.path.append("/usr/src/app")
import falcon
import json
from api.main.util.helpers import getlogger, validate_params, response, produce
from api.main.util import constant as cst
from api.main.service import agent

log = getlogger(cst.LOGGER['AGENT_RESSOURCE'], cst.LOG_FILES["AGENT_RESSOURCE_LOG"])


class Agents(object):
    main_endpoint = "agents"

    def on_get(self, req, resp):
        """Return list of all agents"""
        log.info("Start to get all agents")
        try:
            current_endpoint = self.main_endpoint + "-" + req.method.lower()
            log.info("get agents list ")
            #log.info(payload)
            payload = {}
            #log.info(agent.get_all_agents(payload))
            response_from_service = agent.get_all_agents(payload)
            if response_from_service:
                #resp.status = response_from_service["status"]
                resp.body = response_from_service
                return
        except Exception as e:
            log.info("There is an error on get list of agents: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return

    def on_post(self, req, resp):
        """Handles agents creation by post"""
        log.info("Start agents created")
       
        try:
            current_endpoint = self.main_endpoint + "-" + req.method.lower()
            log.info(req.context['doc'])

            log.info(current_endpoint)
            if validate_params(current_endpoint, req.context['doc']):
                #resp.status = response_from_service["status"]
                error = {
                    "code": cst.ERROR_CODE["MISSING_REQUIRE_PARAMETER"],
                    "params": cst.REQUIRED_FIELDS_MAP[current_endpoint],
                }
                log.info(error)
                resp.body = response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
                return

            log.info("Getting to prepare agent")
            payload = {
                "agent": {
                    "firstname": req.context["doc"].get("firstname"),
                    "lastname": req.context["doc"].get("lastname"),
                    "email": req.context["doc"].get("email"),
                    "password": req.context["doc"].get("password"),
                    "roles": req.context["doc"].get("roles"),
                    "user":req.context["doc"].get("user")
                }
               
            }

            log.info("agent to send")
            log.info(payload)
            response_from_service = agent.create_agent(payload)
            #resp.status = response_from_service["status"]
            resp.body = response_from_service
            return
        except Exception as e:
            log.info("There is an error on post agent: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return


class Agent(object):
    main_endpoint = "agent"

    def on_put(self, req, resp, agent_id):
        """Handles agents update by put"""
        log.info("Start agents edit")
       
        try:
            current_endpoint = self.main_endpoint + "-" + req.method.lower()
            log.info(req.context['doc'])

            log.info(current_endpoint)
            if validate_params(current_endpoint, req.context['doc']):
                resp.status = response_from_service["status"]
                error = {
                    "code": cst.ERROR_CODE["MISSING_REQUIRE_PARAMETER"],
                    "params": cst.REQUIRED_FIELDS_MAP[current_endpoint],
                }
                log.info(error)
                resp.body = response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
                return

            log.info("Getting to prepare agent")
            payload = {
               
                    "agent": {
                    "id": agent_id,
                    "firstname": req.context["doc"].get("firstname"),
                    "lastname": req.context["doc"].get("lastname"),
                    "email": req.context["doc"].get("email"),
                    "password": req.context["doc"].get("password"),
                    "roles": req.context["doc"].get("roles"),
                    "user":req.context["doc"].get("user")                
                }               
            }

            log.info("agent to send")
            log.info(payload)
            response_from_service = agent.update_agent(payload)
            #resp.status = response_from_service["status"]
            resp.body = response_from_service
            return
        except Exception as e:
            log.info("There is an error on post agent: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return

    def on_delete(self, req, resp, agent_id):
        user = req.get_param('user', None)
        log.info("deactivate agent endpoint")
        id = req.get_param('agent', None)
        try:
            payload = {
                "agent": {
                    "id": agent_id,
                    "user": user
                }
            }

            log.info("data to send")
            log.info(payload)
            response_from_service = agent.deactivate_agent(payload)
            #resp.status = response_from_service["status"]
            resp.body = response_from_service
            return
        except Exception as e:
            log.info("There is an error on post agent: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return










#     def on_delete(self, req, resp):
#         """Handles counter delete multiple"""
#         log.info("Start counter delete multiple")
#         try:
#             current_endpoint = self.main_endpoint + "-" + req.method.lower()
#             log.info(req.context['doc'])

#             log.info(current_endpoint)
#             if req.context["doc"].get("counter_ids") is None or not isinstance(req.context["doc"].get("counter_ids"), list) or (isinstance(req.context["doc"].get("counter_ids"),list) and  len(req.context["doc"].get("counter_ids")) <=0 ) :
#                 resp.status = falcon.HTTP_OK
#                 error = {
#                     "code": cst.ERROR_CODE["MISSING_REQUIRE_PARAMETER"],
#                     "params": cst.REQUIRED_FIELDS_MAP[current_endpoint],
#                 }
#                 log.info(error)
#                 resp.body = response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
#                 return

#             log.info("Getting to prepare counter")
#             payload = {
#                 "counter": {
#                     "ids": req.context["doc"].get("counter_ids")
#                 },
#                 "sio_channel": req.context["doc"].get("sio_channel"),
#                 "force": req.context["doc"].get("force", 0),
#                 "user-session": req.context['doc'].get('user'),
#                 "process": cst.PROCESSES["DELETE_MULTI_COUNTERS"]
#             }

#             log.info("counter to delete")
#             log.info(payload)
#             produce(cst.KAFKA_TOPICS["MAIN_TOPIC"], payload)
#             resp.status = falcon.HTTP_OK
#             resp.body = response(cst.HTTP_STATUS["HTTP_202"], cst.ERROR_TITLES["HTTP_202"])
#             return
#         except Exception as e:
#             log.info("There is an error on delete counter: %s" % e)
#             resp.status = falcon.HTTP_200
#             resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
#             return


# class Counter(object):