import sys

sys.path.append("/usr/src/app")
import falcon
import json
from api.main.util.helpers import getlogger, validate_params, response, produce
from api.main.util import constant as cst
from api.main.service import user_activity


log = getlogger(cst.LOGGER['USER_ACTIVITY_RESSOURCE'], cst.LOG_FILES["USER_ACTIVITY_RESSOURCE_LOG"])

class UsersActivities(object):
    main_endpoint = "users_activities"

    def on_post(self, req, resp):
        """Handles userActivities stored by post"""
        log.info("Start userActivities stored")
       
        try:
            current_endpoint = self.main_endpoint + "-" + req.method.lower()
            log.info(req.context['doc'])

            log.info(current_endpoint)
            if validate_params(current_endpoint, req.context['doc']):
                resp.status = falcon.HTTP_OK
                error = {
                    "code": cst.ERROR_CODE["MISSING_REQUIRE_PARAMETER"],
                    "params": cst.REQUIRED_FIELDS_MAP[current_endpoint],
                }
                log.info(error)
                resp.body = response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
                return

            log.info("Getting to prepare agent")
            payload = {
                "logs": req.context["doc"].get("activities")     
            }

            log.info(payload)
            response_from_service = user_activity.store_activities(payload)
            #resp.status = response_from_service["status"]
            resp.body = response_from_service
            return
        except Exception as e:
            log.info("There is an error on post agent: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return

