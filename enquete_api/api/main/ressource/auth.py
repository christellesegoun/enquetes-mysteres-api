import sys

sys.path.append("/usr/src/app")
import falcon
import json
from api.main.util.helpers import getlogger, validate_params, response, produce
from api.main.util import constant as cst
from api.main.service import agent

log = getlogger(cst.LOGGER['AGENT_RESSOURCE'], cst.LOG_FILES["AGENT_RESSOURCE_LOG"])


class Auth(object):
    main_endpoint = "auth"

    #login
    def on_post(self, req, resp):
        """Handles agents login by post"""
        log.info("Start login created")
       
        try:
            current_endpoint = self.main_endpoint + "-" + req.method.lower()
            log.info(req.context['doc'])
            
            log.info(current_endpoint)
            if validate_params(current_endpoint, req.context['doc']):
                #resp.status = response_from_service["status"]
                error = {
                    "code": cst.ERROR_CODE["MISSING_REQUIRE_PARAMETER"],
                    "params": cst.REQUIRED_FIELDS_MAP[current_endpoint],
                }
                log.info(error)
                resp.body = response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
                return

            log.info("Getting to prepare agent")
            payload = {
                "agent": {
                    "email": req.context["doc"].get("email"),
                    "password": req.context["doc"].get("password")
                }
               
            }

            log.info("agent to send")
            log.info(payload)
            response_from_service = agent.login(payload)
            log.info(response_from_service)
            #resp.status = response_from_service["status"]
            resp.body = response_from_service
            return
        except Exception as e:
            log.info("There is an error on post agent: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return


    #logout
    def on_get(self, req, resp):
        """"""
        log.info("logout endpoint")
        
        id = req.get_param('agent', None)
        token = req.get_param('token', None)
        try:
            payload = {
                "agent": {
                    "id": id,
                    "token": token
                }
            }

            log.info("data to send")
            log.info(payload)

            response_from_service = agent.checksessionorlogout(payload)
            
            log.info(response_from_service)
            log.info(type(response_from_service))
            log.info(json.loads(response_from_service))
            js = json.loads(response_from_service)
            #resp.status = js["status"]
            resp.body = response_from_service
            return
        except Exception as e:
            log.info("There is an error on post agent: %s" % e)
            resp.status = falcon.HTTP_500
            resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
            return











#     def on_delete(self, req, resp):
#         """Handles counter delete multiple"""
#         log.info("Start counter delete multiple")
#         try:
#             current_endpoint = self.main_endpoint + "-" + req.method.lower()
#             log.info(req.context['doc'])

#             log.info(current_endpoint)
#             if req.context["doc"].get("counter_ids") is None or not isinstance(req.context["doc"].get("counter_ids"), list) or (isinstance(req.context["doc"].get("counter_ids"),list) and  len(req.context["doc"].get("counter_ids")) <=0 ) :
#                 resp.status = falcon.HTTP_OK
#                 error = {
#                     "code": cst.ERROR_CODE["MISSING_REQUIRE_PARAMETER"],
#                     "params": cst.REQUIRED_FIELDS_MAP[current_endpoint],
#                 }
#                 log.info(error)
#                 resp.body = response(cst.HTTP_STATUS["HTTP_400"], cst.ERROR_TITLES["HTTP_400"], error=error)
#                 return

#             log.info("Getting to prepare counter")
#             payload = {
#                 "counter": {
#                     "ids": req.context["doc"].get("counter_ids")
#                 },
#                 "sio_channel": req.context["doc"].get("sio_channel"),
#                 "force": req.context["doc"].get("force", 0),
#                 "user-session": req.context['doc'].get('user'),
#                 "process": cst.PROCESSES["DELETE_MULTI_COUNTERS"]
#             }

#             log.info("counter to delete")
#             log.info(payload)
#             produce(cst.KAFKA_TOPICS["MAIN_TOPIC"], payload)
#             resp.status = falcon.HTTP_OK
#             resp.body = response(cst.HTTP_STATUS["HTTP_202"], cst.ERROR_TITLES["HTTP_202"])
#             return
#         except Exception as e:
#             log.info("There is an error on delete counter: %s" % e)
#             resp.status = falcon.HTTP_200
#             resp.body = response(cst.HTTP_STATUS["HTTP_500"], cst.ERROR_TITLES["HTTP_500"])
#             return


# class Counter(object):