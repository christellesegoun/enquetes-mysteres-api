
#--------- Mongo connection config ------------#
MONGO_HOST = 'enquete_mongo_db'
MONGO_PORT = 27017
MONGO_URL = ""
MONGO_DB_NAME = 'publicdb'
AGENT_COLLECTION = 'agents'
SURVEY_COLLECTION_SYNCED = 'surveys'
SURVEY_COLLECTION_VALID = 'surveys_valid'
SURVEY_COLLECTION_REJECTED = 'surveys_rejected',
ACTIVITY_COLLECTION = 'activities'

#--------- KAFKA CONFIG ------#
KAFKA_BROKER_URL = "kafka-righq-node1:9092,kafka-righq-node2:9092"
KAFKA_LOG_BROKER_URL =  "10.10.14.121:9092,10.10.14.121:9090"
KAFKA_CONSUMER_SESSION_TIMEOUT = 10000
KAFKA_QUEUED_MAX_MESSAGE_KB = 2000000
KAFKA_AUTO_OFFSET_RESET = 'smallest'
KAFKA_PRODUCER_CONFIGURATION = {
    'bootstrap.servers': KAFKA_BROKER_URL
    #'linger.ms': 0,
    #'compression.type': None,
    #'request.required.acks': 1,
    #'retries': 1,
    #'max.in.flight.requests.per.connection': 1
}
KAFKA_LOG_PRODUCER_CONFIGURATION = {
    'bootstrap.servers': KAFKA_LOG_BROKER_URL
    #'linger.ms': 0,
    #'compression.type': None,
    #'request.required.acks': 1,
    #'retries': 1,
    #'max.in.flight.requests.per.connection': 1
}

STATUS_SURVEY = {
    0: "SYNCED",
    1: "",
    2: "",
    3: "REJECTED"
}

#---------- REDIS CONFIG --------#
REDIS_HOST = "10.10.14.121"
REDIS_PORT = 9080
REDIS_URL = 'redis://'+REDIS_HOST+':'+str(REDIS_PORT)



#---------- REDIS SOCKET CONFIG --------#
REDIS_SOCKET_HOST = "10.10.14.116"
REDIS_SOCKET_PORT = 9999
REDIS_SOCKET_URL = 'redis://'+REDIS_SOCKET_HOST+':'+str(REDIS_PORT)



#-------- API DATA ----------#

REQUEST_MAX_LIMIT = 500
REDIS_KEY_SEPARATOR = "-------"

HTTP_STATUS = {
    "HTTP_400": 400,
    "HTTP_500": 500,
    "HTTP_202": 202,
    "HTTP_200": 200
}

ERROR_TITLES = {
    "HTTP_400": "Forbidden",
    "HTTP_500": "Unhandled Exception",
    "HTTP_202": "Accepted",
    "HTTP_200": "Ok"
}


ERROR_CODE = {
    "MISSING_REQUIRE_PARAMETER": 1001,
    "DB_CONNEXION_ERROR": 1002,
    "INVALID_EMAIL":1003,
    "EMAIL_EXIST":1004,
    "AGENT_NOT_FOUND":1005,
    "INVALID_PASSWORD":1006,
    "INVALID_FORMAT":1007,
    "AGENT_SESSION_NOT_VALID":1008,
    "TOKEN_EXIST":1009,
    "MISSING_SESSION":1010,
    "AGENT_NOT_FOUND":1011,
    "AGENT_NOT_ACTIVE":1012

}

REQUIRED_FIELDS_MAP = {
    'agents-post': ["firstname", "lastname", "email", "password", "roles", "user"],
    'auth-post' : ["email", "password"],
    'surveys-post': ["surveys", "agent"],
    'agent-put': ["firstname", "lastname", "email", "roles", "user"],
    'users_activities-post': ["activities"]
}


HMAP = {
    "agent_tok": "AGENTS_TOKEN",
    "agent_pass": "AGENTS_PASSWORD"
}


DEFAULT_VALUES = {
    "SERVICE-DEFAULT-COLOR": "GREEN",
    "SERVICE-DEFAULT-PRIORITY": 0,
    "SERVICE-DEFAULT-ACTIVE-STATE": 1,
    "STR_EMPTY": "",
    "INT_EMPTY": 0,
    "SERVICE_DEFAULT_IMAGE": "ttttest",
    "ACTIVE_DEFAULT":True
}


KAFKA_TOPICS = {
    "MAIN_TOPIC" : "rightq",
    "UPLOAD_TOPIC": "uploads"
}

STATUS_TICKET =  {
    0: "PENDING" ,
    1: "CLOSED",
    2: "MISSING",
    3: "TRANSFERRED"

}

PROCESSES = {
    "CREATE_SERVICE": "create_service",
    "GET_SERVICE": "get_specific_service",
    "UPDATE_SERVICE": "update_one_service",
    "GET_SERVICES": "get_list_of_all_services",
    "DELETE_SERVICE": "delete_service",
    "GET_SERVICE_KIOSK": "get_service_kiosk",
    "DELETE_MULTI_SERVICES": "delete_multi_services",

    "CREATE_SUB_SERVICE": "create_sub_service",
    "GET_SUB_SERVICE": "get_specific_sub_service",
    "UPDATE_SUB_SERVICE": "update_one_sub_service",
    "GET_SUB_SERVICES": "get_list_of_all_sub_services",
    "DELETE_SUB_SERVICE": "delete_sub_service",
    "GET_SUBSERVICE_KIOSK": "get_subservice_kiosk",
    "DELETE_MULTI_SUBSERVICES": "delete_multi_subservices",

    "CREATE_COUNTER": "create_counter",
    "GET_COUNTER": "get_specific_counter",
    "UPDATE_COUNTER": "update_one_counter",
    "GET_COUNTERS": "get_list_of_all_counters",
    "DELETE_COUNTER": "delete_counter",
    "DELETE_MULTI_COUNTERS": "delete_multi_counters",

    "CREATE_EQUIPMENT": "create_equipment",
    "GET_EQUIPMENT": "get_specific_equipment",
    "UPDATE_EQUIPMENT": "update_one_equipment",
    "GET_EQUIPMENTS": "get_list_of_all_equipments",
    "DELETE_EQUIPMENT": "delete_equipment",
    "DELETE_MULTI_EQUIPMENTS": "delete_multi_equipments",

    "CREATE_CUSTOMER": "create_customer",
    "GET_CUSTOMER": "get_specific_customer",
    "UPDATE_CUSTOMER": "update_one_customer",
    "GET_CUSTOMERS": "get_list_of_all_customers",
    "DELETE_CUSTOMER": "delete_customer",
    "DELETE_MULTI_CUSTOMERS": "delete_multi_customers",

    "CREATE_GROUP": "create_group",
    "GET_GROUP": "get_specific_group",
    "UPDATE_GROUP": "update_one_group",
    "DELETE_GROUP": "delete_group",
    "GET_GROUPS": "get_list_of_all_groups",
    "DELETE_MULTI_GROUPS": "delete_multi_groups",

    "CREATE_TAG": "create_tag",
    "GET_TAG": "get_specific_tag",
    "DELETE_TAG": "delete_tag",
    "GET_TAGS": "get_list_of_all_tags",

    "CREATE_QUEUE": "create_queue",
    "GET_QUEUE": "get_specific_queue",
    "UPDATE_QUEUE": "update_one_queue",
    "GET_QUEUES": "get_list_of_all_queues",
    "DELETE_QUEUE": "delete_queue",
    "DELETE_MULTI_QUEUES": "delete_multi_queues",

    "GET_SUBSCRIPTION": "get_subscription",

    "ASSIGN_QUEUE_SERVICE": "assign_queue_service",

    "GET_USERS": "get_users",

    "ASSIGN_QUEUE_USER": "assign_queue_user",

    "ASSIGN_QUEUE_COUNTER": "assign_queue_counter",

    "ASSIGN_COUNTER_SERVICE": "assign_counter_service",

    "CREATE_PARCOUR": "create_parcours",
    "GET_PARCOURS": "get_parcours",
    "UPDATE_PARCOUR": "update_parcours",
    "DELETE_PARCOUR": "delete_parcour",
    "GET_PARCOUR": "get_parcour",
    "DELETE_MULTI_PARCOURS": "delete_multi_parcours",

    "GET_KIOSK_TEMPLATES": "get_kiosk_template",

    "SERVICE_HOUR": "service_hour",

    "GET_SETTINGS": "get-settings",
    "CONFIGURE_SETTING": "configure-setting",

    "CREATE_KIOSK_TEMPLATE": "create_kiosk_template",
    "GET_KIOSK_TEMPLATE": "get_specific_kiosk_template",
    "UPDATE_KIOSK_TEMPLATE": "update_one_kiosk_template",
    "DELETE_KIOSK_TEMPLATE": "delete_kiosk_template",
    "DELETE_MULTI_TEMPLATES": "delete_multi_templates",

    "CREATE_TICKET": "create_ticket",
    "UPLOAD": "customer_list"



}

LOGGER = {
    "AGENT_RESSOURCE": "agent_ressources", 
    "AGENT_LOGIC": "agent_logic",
    "SURVEY_RESSOURCE": "survey_ressources", 
    "SURVEY_LOGIC": "survey_logic",
    "USER_ACTIVITY_RESSOURCE": "user_activity_ressources",
    "USER_ACTIVITY_LOGIC": "user_activity_logic"

}

LOG_FILES = {

    "AGENT_RESSOURCE_LOG": "/usr/src/app/logs/ressource_agent.log",
    "AGENT_LOGIC_LOG": "/usr/src/app/logs/agent_logic.log",
    "SURVEY_RESSOURCE_LOG": "/usr/src/app/logs/ressource_survey.log",
    "SURVEY_LOGIC_LOG": "/usr/src/app/logs/survey_logic.log",
    "USER_ACTIVITY_RESSOURCE_LOG": "/usr/src/app/logs/ressource_user_activity.log",
    "USER_ACTIVITY_LOGIC_LOG": "/usr/src/app/logs/user_activity_logic.log"

}

GROUP_COLUMN_FAMILY_SEPARATOR =  "_"
GROUP_COLUMN_FAMILY = {
    "SERVICE": "service",
    "COUNTER": "counter",
    "CUSTOMER": "customer",
    "EQUIPMENT": "equipment",
    "QUEUE": "queue"
}


UPLOADS_TYPE =  ["customer_list"]

#--------- SOCKETIO ------------#
SOCKET_NAMESPACE = '/rq'
SOCKET_PORT = 612



#-------- ACCOUNT FAKE DATA-------#
DEFAULT_TEST_USER_ACCOUNT_ID = "AAZZE"


#-------- CONSTANT LOGS ----------#
APPID = "rightq"
LOG_SOURCE = "API-PL"
TEST_CLIENT_ID = "RD7E86N34SAN"
BASE_DB_NAME = "behanzin_"
APPTOKEN = "8ded845c-f415-11e8-b28d-0242ac160003"
LOG_KAFKA_TOPIC = "log_q"
LOG_KAFKA_PROCESS = "insert_log"
LOG_EVENT = {
    "SERVICE_CREATE_EVENT": "ADD_SERVICE_SUCCESS",
    "SERVICE_CREATE_ERROR_EVENT": "ADD_SERVICE_ERROR",
    "SERVICE_UPDATE_EVENT": "UPDATE_SERVICE_SUCCESS",
    "SERVICE_UPDATE_ERROR_EVENT": "UPDATE_SERVICE_ERROR",
    "SERVICE_DELETE_EVENT": "DELETE_SERVICE_SUCCESS",
    "SERVICE_DELETE_ERROR_EVENT": "DELETE_SERVICE_ERROR",
    "SERVICE_DELETE_MULTI_EVENT": "DELETE_SERVICE_MULTI_SUCCESS",
    "SERVICE_DELETE_MULTI_ERROR_EVENT": "DELETE_SERVICE_MULTI_ERROR",

    "QUEUE_CREATE_EVENT": "ADD_QUEUE_SUCCESS",
    "QUEUE_CREATE_ERROR_EVENT": "ADD_QUEUE_ERROR",
    "QUEUE_UPDATE_EVENT": "UPDATE_QUEUE_SUCCESS",
    "QUEUE_UPDATE_ERROR_EVENT": "UPDATE_QUEUE_ERROR",
    "QUEUE_DELETE_EVENT": "DELETE_QUEUE_SUCCESS",
    "QUEUE_DELETE_ERROR_EVENT": "DELETE_QUEUE_ERROR",
    "QUEUE_DELETE_MULTI_EVENT": "DELETE_QUEUE_MULTI_SUCCESS",
    "QUEUE_DELETE_MULTI_ERROR_EVENT": "DELETE_QUEUE_MULTI_ERROR",

    "COUNTER_CREATE_EVENT": "ADD_COUNTER_SUCCESS",
    "COUNTER_CREATE_ERROR_EVENT": "ADD_COUNTER_ERROR",
    "COUNTER_UPDATE_EVENT": "UPDATE_COUNTER_SUCCESS",
    "COUNTER_UPDATE_ERROR_EVENT": "UPDATE_COUNTER_ERROR",
    "COUNTER_DELETE_EVENT": "DELETE_COUNTER_SUCCESS",
    "COUNTER_DELETE_ERROR_EVENT": "DELETE_COUNTER_ERROR",
    "COUNTER_DELETE_MULTI_EVENT": "DELETE_COUNTER_MULTI_SUCCESS",
    "COUNTER_DELETE_MULTI_ERROR_EVENT": "DELETE_COUNTER_MULTI_ERROR",

    "EQUIPMENT_CREATE_EVENT": "ADD_EQUIPMENT_SUCCESS",
    "EQUIPMENT_CREATE_ERROR_EVENT": "ADD_EQUIPMENT_ERROR",
    "EQUIPMENT_UPDATE_EVENT": "UPDATE_EQUIPMENT_SUCCESS",
    "EQUIPMENT_UPDATE_ERROR_EVENT": "UPDATE_EQUIPMENT_ERROR",
    "EQUIPMENT_DELETE_EVENT": "DELETE_EQUIPMENT_SUCCESS",
    "EQUIPMENT_DELETE_ERROR_EVENT": "DELETE_EQUIPMENT_ERROR",
    "EQUIPMENT_DELETE_MULTI_ERROR_EVENT": "DELETE_EQUIPMENT_MULTI_ERROR",
    "EQUIPMENT_DELETE_MULTI_EVENT": "DELETE_EQUIPMENT_MULTI_SUCCESS",

    "CUSTOMER_CREATE_EVENT": "ADD_CUSTOMER_SUCCESS",
    "CUSTOMER_CREATE_ERROR_EVENT": "ADD_CUSTOMER_ERROR",
    "CUSTOMER_UPDATE_EVENT": "UPDATE_CUSTOMER_SUCCESS",
    "CUSTOMER_UPDATE_ERROR_EVENT": "UPDATE_CUSTOMER_ERROR",
    "CUSTOMER_DELETE_EVENT": "DELETE_CUSTOMER_SUCCESS",
    "CUSTOMER_DELETE_ERROR_EVENT": "DELETE_CUSTOMER_ERROR",
    "CUSTOMER_DELETE_MULTI_ERROR_EVENT": "DELETE_CUSTOMER_MULTI_ERROR",
    "CUSTOMER_DELETE_MULTI_EVENT": "DELETE_CUSTOMER_MULTI_SUCCESS",

    "SUBSERVICE_CREATE_EVENT": "ADD_SUBSERVICE_SUCCESS",
    "SUBSERVICE_CREATE_ERROR_EVENT": "ADD_SUBSERVICE_ERROR",
    "SUBSERVICE_UPDATE_EVENT": "UPDATE_SUBSERVICE_SUCCESS",
    "SUBSERVICE_UPDATE_ERROR_EVENT": "UPDATE_SUBSERVICE_ERROR",
    "SUBSERVICE_DELETE_EVENT": "DELETE_SUBSERVICE_SUCCESS",
    "SUBSERVICE_DELETE_ERROR_EVENT": "DELETE_SUBSERVICE_ERROR",
    "SUBSERVICE_DELETE_MULTI_ERROR_EVENT": "DELETE_SUBSERVICE_MULTI_ERROR",
    "SUBSERVICE_DELETE_MULTI_EVENT": "DELETE_SUBSERVICE_MULTI_SUCCESS",


    "GROUP_CREATE_EVENT": "ADD_GROUP_SUCCESS",
    "GROUP_CREATE_ERROR_EVENT": "ADD_GROUP_ERROR",
    "GROUP_UPDATE_EVENT": "UPDATE_GROUP_SUCCESS",
    "GROUP_UPDATE_ERROR_EVENT": "UPDATE_GROUP_ERROR",
    "GROUP_DELETE_EVENT": "DELETE_GROUP_SUCCESS",
    "GROUP_DELETE_ERROR_EVENT": "DELETE_GROUP_ERROR",
    "GROUP_DELETE_MULTI_ERROR_EVENT": "DELETE_GROUP_MULTI_ERROR",
    "GROUP_DELETE_MULTI_EVENT": "DELETE_GROUP_MULTI_SUCCESS",

    "PARCOURS_CREATE_EVENT": "ADD_PARCOURS_SUCCESS",
    "PARCOURS_CREATE_ERROR_EVENT": "ADD_PARCOURS_ERROR",
    "PARCOURS_UPDATE_EVENT": "UPDATE_PARCOURS_SUCCESS",
    "PARCOURS_UPDATE_ERROR_EVENT": "UPDATE_PARCOUR_ERROR",
    "PARCOURS_DELETE_EVENT": "DELETE_PARCOURS_SUCCESS",
    "PARCOURS_DELETE_ERROR_EVENT": "DELETE_PARCOURS_ERROR",
    "PARCOURS_DELETE_MULTI_ERROR_EVENT": "DELETE_PARCOURS_MULTI_ERROR",
    "PARCOURS_DELETE_MULTI_EVENT": "DELETE_PARCOURS_MULTI_SUCCESS",

    "KIOSK_TEMPLATE_CREATE_EVENT": "ADD_KIOSK_TEMPLATE_SUCCESS",
    "KIOSK_TEMPLATE_CREATE_ERROR_EVENT": "ADD_KIOSK_TEMPLATE_ERROR",
    "KIOSK_TEMPLATE_UPDATE_EVENT": "UPDATE_KIOSK_TEMPLATE_SUCCESS",
    "KIOSK_TEMPLATE_UPDATE_ERROR_EVENT": "UPDATE_KIOSK_TEMPLATE_ERROR",
    "KIOSK_TEMPLATE_DELETE_EVENT": "DELETE_KIOSK_TEMPLATE_SUCCESS",
    "KIOSK_TEMPLATE_DELETE_ERROR_EVENT": "DELETE_KIOSK_TEMPLATE_ERROR",
    "KIOSK_DELETE_MULTI_ERROR_EVENT": "DELETE_KIOSK_MULTI_ERROR",
    "KIOSK_TEMPLATE_DELETE_MULTI_EVENT": "DELETE_KIOSK_TEMPLATE_MULTI_SUCCESS",

    "ASSIGN_SERVICE_TO_QUEUE_EVENT": "ASSIGN_SERVICE_TO_QUEUE_SUCCESS",
    "ASSIGN_SERVICE_TO_QUEUE_ERROR_EVENT": "ASSIGN_SERVICE_TO_QUEUE_ERROR",

    "ASSIGN_COUNTER_TO_QUEUE_EVENT": "ASSIGN_COUNTER_TO_QUEUE_SUCCESS",
    "ASSIGN_COUNTER_TO_QUEUE_ERROR_EVENT": "ASSIGN_COUNTER_TO_QUEUE_ERROR",

    "ASSIGN_USER_TO_QUEUE_EVENT": "ASSIGN_USER_TO_QUEUE_SUCCESS",
    "ASSIGN_USER_TO_QUEUE_ERROR_EVENT": "ASSIGN_USER_TO_QUEUE_ERROR",

    "ASSIGN_SERVICE_TO_COUNTER_EVENT": "ASSIGN_SERVICE_TO_COUNTER_SUCCESS",
    "ASSIGN_SERVICE_TO_COUNTER_ERROR_EVENT": "ASSIGN_SERVICE_TO_COUNTER_ERROR",

    "TAG_CREATE_EVENT": "ADD_TAG_SUCCESS",
    "TAG_CREATE_ERROR_EVENT": "ADD_TAG_ERROR",
    "TAG_DELETE_EVENT": "DELETE_TAG_SUCCESS",
    "TAG_DELETE_ERROR_EVENT": "DELETE_TAG_ERROR",

    "SETTING_UPDATE_EVENT": "SETTING_UPDATE_SUCCESS",
    "SETTING_UPDATE_ERROR_EVENT" : "SETTING_UPDATE_ERROR",

    "TICKET_CREATE_EVENT": "ADD_TICKET_SUCCESS",
    "TICKET_CREATE_ERROR_EVENT": "ADD_TICKET_ERROR"

}

USER_SUBSCRIPTION_TYPE = ["Lite", "Standard", "Professional", "Enterprise", "QLib Free", "QLib"]
USER_BY_SUBS = "user_by_sub"


#EXTERNAL API'S ENDPOINT
ADMIN_BASE_URL = "http://10.10.14.116:19009/api/v2/service"
GET_USERS_ENDPOINT = "/app_users/list"
GET_USERS_SESSION = "/app_sessions/session"
DEFAULT_ALIAS = "rightcom"
LICENSE_DEFAULT_VALIDITY = 50
CDN_BASE_URL = "http://cdn.right-com.com/api/1.0"
DOWNLOAD_FILE_ENDPOINT = "/download"
