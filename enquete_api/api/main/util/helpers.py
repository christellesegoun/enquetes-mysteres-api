
import sys
sys.path.append("/usr/src/app")
import json
import os
import hashlib
import logging
import random
import uuid
import falcon
import requests
import datetime
import redis
import string
from api.main.util import constant as cst
from threading import Thread
from multiprocessing import Process
from slackclient import SlackClient
from pymongo import MongoClient, errors
#from uwsgidecorators import *



r_pool = redis.ConnectionPool(host=cst.REDIS_HOST, port=cst.REDIS_PORT, db=1)
#kafka_producer = Producer(cst.KAFKA_PRODUCER_CONFIGURATION)


def connect_to_db_mongo(collection):
    try:
        client = MongoClient(cst.MONGO_HOST, cst.MONGO_PORT)
        db = client[cst.MONGO_DB_NAME]
        if collection:
            return 1, db[collection]
            
        return 1, ""
    except errors.ServerSelectionTimeoutError as err:
        print("there is an error %s" %err)
        return 0,"DB_CONNEXION_FAILED"


#check if key exist in redis
def redis_key_exist_in_hmap(hmap, key):
    try:
        log.info("Start checking key existence")
        my_redis = redis.Redis(connection_pool=r_pool)
        log.info(my_redis.hexists(hmap, key))
        return my_redis.hexists(hmap, key)
    except Exception as e:
        print("there is an error %s" %e)

#Add key to redis for cach retrieval
def add_to_redis_in_hmap(hmap, data):
    my_redis = redis.Redis(connection_pool=r_pool)
    my_redis.hset(hmap, data["KEY"], data["VALUE"])
    
    return True

def redis_get_value_hmap(hmap, key):
    my_redis = redis.Redis(connection_pool=r_pool)
    res = my_redis.hget(hmap, key)
    return res.decode('utf-8') if res != None else res

#Encrypt password
def encrypt_pass(password):
    #salt = os.urandom(32) # A new salt for this user
    key = hashlib.sha224(password.encode('utf-8')).hexdigest()
    return key

#connect to a cassandra keyspace (database)

class AuthMiddleware(object):

    def process_request(self, req, resp):
        publickey = req.get_header('publickey')
        apisid = req.get_header('apisid')
        sessionid = req.get_header('sessionid')
        authorize_route = ["/rightq/utils/api/v2/database", "/rightq/utils/api/v2/license"]

        challenges = ['Token type="keys"']
        description = ('Please provide an auth key '
                       'as part of the request.')
        print(req.path)
        if req.path in authorize_route:
            return True

        if publickey is None or apisid is None or sessionid is None:


            raise falcon.HTTPUnauthorized('Auth token required',
                                          description,
                                          challenges,
                                          href='https://docs.google.com/document/d/18JME6JHKdwjmaDcZjiATRI0AEEz8WcC-ccbFipqB01o/edit')

        res_auth = self._session_is_valid(publickey, apisid, sessionid)
        if not res_auth[0]:
            raise falcon.HTTPUnauthorized('Auth token required',
                                          description,
                                          challenges,
                                          href='https://docs.google.com/document/d/18JME6JHKdwjmaDcZjiATRI0AEEz8WcC-ccbFipqB01o/edit')

        if "doc" not in req.context:
            req.context['doc'] = {}

        req.context['doc']['user'] = res_auth[1]




    def _session_is_valid(self, publickey, apisid, sessionid):
        print("Getting sessions from admin")
        # TO DO | Normaly must consider user timezone and GET User id from account Token
        print("Getting all subscription type ....")
        users = []
        admin_session_url = cst.ADMIN_BASE_URL + cst.GET_USERS_SESSION
        print(admin_session_url)
        payload = {
            "publickey": publickey,
            "apisid": apisid,
            "sessionid": sessionid,
            "appid": cst.APPID
        }
        r = requests.get(admin_session_url, params=payload, timeout=10)
        print(r)
        ret_data = r.json()
        if ret_data["status"] == 200:
            return True, ret_data["data"]
        return False, {}

class RequireJSON(object):

    def process_request(self, req, resp):
        if not req.client_accepts_json:
            raise falcon.HTTPNotAcceptable(
                'This API only supports responses encoded as JSON.',
                href='http://docs.examples.com/api/json')

        if req.method in ('POST', 'PUT'):
            if 'application/json' not in req.content_type:
                raise falcon.HTTPUnsupportedMediaType(
                    'This API only supports requests encoded as JSON.',
                    href='http://docs.examples.com/api/json')


class JSONTranslator(object):
    # NOTE: Starting with Falcon 1.3, you can simply
    # use req.media and resp.media for this instead.

    def process_request(self, req, resp):
        # req.stream corresponds to the WSGI wsgi.input environ variable,
        # and allows you to read bytes from the request body.
        #
        # See also: PEP 3333
        if req.content_length in (None, 0):
            # Nothing to do
            return

        body = req.stream.read()
        if not body:
            raise falcon.HTTPBadRequest('Empty request body',
                                        'A valid JSON document is required.')

        try:
            req.context['doc'] = json.loads(body.decode('utf-8'))

        except (ValueError, UnicodeDecodeError):
            raise falcon.HTTPError(falcon.HTTP_753,
                                   'Malformed JSON',
                                   'Could not decode the request body. The '
                                   'JSON was incorrect or not encoded as '
                                   'UTF-8.')

class CORSComponent(object):

    def process_request(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header('Access-Control-Allow-Methods', 'DELETE, PUT, POST, GET')
        resp.set_header('Access-Control-Allow-Headers', 'content-type, apisid, publickey, sessionid')
        resp.set_header('Access-Control-Expose-Headers', '*')
        resp.set_header('Access-Control-Max-Age', 1728000)  # 20 days
        if req.method == 'OPTIONS':
            raise falcon.HTTPStatus(falcon.HTTP_200, body='\n')
        # resp.set_header('Access-Control-Allow-Origin', '*')
        #
        # if (req_succeeded
        #         and req.method == 'OPTIONS'
        #         and req.get_header('Access-Control-Request-Method')
        # ):
        #     # NOTE(kgriffs): This is a CORS preflight request. Patch the
        #     #   response accordingly.
        #
        #     allow = resp.get_header('Allow')
        #     resp.delete_header('Allow')
        #
        #     allow_headers = req.get_header(
        #         'Access-Control-Request-Headers',
        #         default='*'
        #     )
        #
        #     resp.set_headers((
        #         ('Access-Control-Allow-Methods', allow),
        #         ('Access-Control-Allow-Headers', allow_headers),
        #         ('Access-Control-Max-Age', '86400'),  # 24 hours
        #     ))



    def process_response(self, req, resp, resource, req_succeeded):
        if 'result' not in resp.context:
            return

        resp.body = json.dumps(resp.context['result'])


def max_body(limit):

    def hook(req, resp, resource, params):
        length = req.content_length
        if length is not None and length > limit:
            msg = ('The size of the request is too large. The body must not '
                   'exceed ' + str(limit) + ' bytes in length.')

            raise falcon.HTTPRequestEntityTooLarge(
                'Request body is too large', msg)

    return hook

#log creator
def getlogger(name, log_file, log_level = "INFO"):

    log_filename = datetime.datetime.now().strftime('%Y-%m-%d') + '.log'
    importer_logger = logging.getLogger(name)
    importer_logger.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s - %(msecs)d - %(funcName)s - %(lineno)d : %(levelname)s : %(message)s')

    fh = logging.FileHandler(filename=log_file)
    fh.setLevel(log_level)
    fh.setFormatter(formatter)
    importer_logger.addHandler(fh)

    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(log_level)
    sh.setFormatter(formatter)
    importer_logger.addHandler(sh)

    return importer_logger



log = getlogger("rightq_helpers", '/usr/src/app/logs/helpers.log')


#require parameter validator
def validate_params(endpoint, data):
    required_fields = cst.REQUIRED_FIELDS_MAP.get(endpoint)
    return [field for field in required_fields if not data.get(field, None)]

#data producer for kafka
def produce(topic, data):
    kafka_producer.produce(topic, json.dumps(data).encode('utf-8'))
    kafka_producer.flush()

#api response template
def response(status, title, data=None, error=None):
    return json.dumps({
                    "status": status,
                    "title": title,
                    "error": error,
                    "data": data
                })


#check if key exist in redis
def redis_key_exist(key):
    try:

        log.info("Start checking key existence")
        my_redis = redis.Redis(connection_pool=r_pool)
        return my_redis.exists(key)
    except Exception as e:
        log.info("there is an error %s" %e)

#Add key to redis for cach retrieval
def add_to_redis(key, value, t=0):
    try:
        log.info(key)
        log.info(value)
        my_redis = redis.Redis(connection_pool=r_pool)
        my_redis.set(str(key), str(value))
        if t != 0:
            my_redis.expire(str(key), t)
        return True
    except Exception as e:
        log.info("This went wrong %s" %e)
        return False

#get a redis key content
def redis_get_value(key):
    my_redis = redis.Redis(connection_pool=r_pool)
    res = my_redis.get(key)
    return res.decode('utf-8') if res != None else res


#delete a key from redis
def redis_del_key(key):
    my_redis = redis.Redis(connection_pool=r_pool)
    return my_redis.delete(key)

#generate code key
def get_code(size=6):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(size))

def check_key_exist_in_redis(keyspace, column_family, **kwargs):
    if keyspace != "" and column_family != "" and len(kwargs) >0:
        log.info(kwargs)
        for key, value in kwargs.items():
            key_to_find = keyspace + cst.REDIS_KEY_SEPARATOR + column_family + cst.REDIS_KEY_SEPARATOR + key + cst.REDIS_KEY_SEPARATOR + value
            log.info(key_to_find)
            log.info(redis_key_exist(key_to_find))
            if redis_key_exist(key_to_find):
                log.info(key_to_find)
                return True, {key: value}

    return False

def delete_keys_in_redis(keyspace, column_family, id=None, **kwargs):
    log.info("Redis delete triggered")
    if keyspace != "" and column_family != "" and len(kwargs) >0:
        for key, value in kwargs.items():
            key_to_find = keyspace + cst.REDIS_KEY_SEPARATOR + column_family + cst.REDIS_KEY_SEPARATOR + key + cst.REDIS_KEY_SEPARATOR + value
            log.info(key_to_find)
            redis_del_key(key_to_find)
            if id is not None:
                key_to_find = keyspace + cst.REDIS_KEY_SEPARATOR + column_family + cst.REDIS_KEY_SEPARATOR + key + cst.REDIS_KEY_SEPARATOR + str(id)
                log.info(key_to_find)
                redis_del_key(key_to_find)
    return True

def set_key_in_redis(keyspace, column_family, id=None, **kwargs):
    try:
        if keyspace != "" and column_family != "" and len(kwargs) > 0:
            log.info(kwargs)
            log.info(id)
            for key,value in kwargs.items():
                key_to_save = keyspace + cst.REDIS_KEY_SEPARATOR + column_family + cst.REDIS_KEY_SEPARATOR + key + cst.REDIS_KEY_SEPARATOR + value
                log.info("key_to_save")
                log.info(key_to_save)
                key_to_save_value = value
                log.info(id)
                log.info(value)
                if id != None:
                    key_to_save_value = str(id)
                log.info("There is no error")
                add_to_redis(key_to_save, key_to_save_value)
                log.info("added first data to redis")
                if id != None:
                    key_to_save_second = keyspace + cst.REDIS_KEY_SEPARATOR + column_family + cst.REDIS_KEY_SEPARATOR + key + cst.REDIS_KEY_SEPARATOR + str(id)
                    log.info("key_to_save2")
                    log.info(key_to_save_second)
                    add_to_redis(key_to_save_second, value)
        log.info("Done with insertion into redis")
        return True
    except Exception as e:
        log.info("This error happened %s" %e)
        return False



def get_keys_values_in_redis(keyspace, column_family, **kwargs):
    log.info("in function")
    log.info(kwargs)
    toret = {}
    if keyspace != "" and column_family != "" and len(kwargs) >0:
        for key, value in kwargs.items():
            key_to_find = keyspace + cst.REDIS_KEY_SEPARATOR + column_family + cst.REDIS_KEY_SEPARATOR + key + cst.REDIS_KEY_SEPARATOR + value
            log.info(key_to_find)
            toret[key] = redis_get_value(key_to_find)
            log.info(toret)
    return toret

def format_num(num):
    log.info(num)
    if num < 10:
        return "00" + str(num)
    else:
        if num < 100:
            return "0" + str(num)
        else:
            return  str(num)



def notifiy_errors_by_mail(service_info, failed_message, payload):
    content = None
    try:
        #Set Mail address to send and receive
        mail_from = "support@rightcomtech.com"
        mail_to = "divyam@right-com.com, christelle@right-com.com"

        #Connecting to SMTP Server
        s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        s.login("support@rightcomtech.com","jrdulurkgclezdyl")

        #Setting up Message
        msg = MIMEMultipart('alternative')
        msg['Subject'] = error + " : " + service
        msg['From'] = mail_from
        msg['To'] = mail_to
        message = json.dumps(data)
        msg.attach(MIMEText(message, 'plain'))

        #Sending mail and closing connection
        s.sendmail(mail_from, mail_to.split(","), msg.as_string())
        s.quit()
        return True
    except Exception as e:
        log.info('An exception was raise when sending notification error: %s' % e)


def sendmail(service, error, data):
    #Set Mail address to send and receive
    mail_from = "support@rightcomtech.com"
    mail_to = "divyam@right-com.com, christelle@right-com.com"

    #Connecting to SMTP Server
    s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    s.login("support@rightcomtech.com","jrdulurkgclezdyl")

    #Setting up Message
    msg = MIMEMultipart('alternative')
    msg['Subject'] = error + " : " + service
    msg['From'] = mail_from
    msg['To'] = mail_to
    message = json.dumps(data)
    msg.attach(MIMEText(message, 'plain'))

    #Sending mail and closing connection
    s.sendmail(mail_from, mail_to.split(","), msg.as_string())
    s.quit()




def notifiy_errors(service_info, failed_message, payload):
    content = None
    try:
        # preparing message to fire error
        log.info('This is ready to fire errors')
        mail_text = (
            'EnqueteMystere: A fail has just been noticed on service: {} with error: {}. Check it out now Urgent. extra info {}').format(
            str(service_info), failed_message, str(payload))
        token = 'xoxb-253919825440-534991669874-utOpYAmX6D9BH8NYKaqYS11k'
        sc = SlackClient(token)
        log.info(mail_text)
        result = sc.api_call('chat.postMessage', channel="enquete-mystere-errors",
                             text=mail_text, username='Christelle-Bot',
                             icon_emoji=':robot_face:')
        log.info(result)
        log.info("message sent already")
        return True
    except Exception as e:
        log.info('An exception was raise when sending notification error: %s' % e)


def download_file(file_id, log, extension ='csv'):
    try:
        file_extension_accepted = ["csv", "xls", "xlsx"]
        log.info('download function')
        to_download = {
            "file_id": file_id,
            "as_attachment": 0,
            "app_name": 'RightQ'
        }
        cdn_download_url = cst.CDN_BASE_URL + cst.DOWNLOAD_FILE_ENDPOINT
        log.info("file to download info")
        log.info(to_download)
        r = requests.get(cdn_download_url, params=to_download, stream=True)
        log.info(r.content)
        path = os.path.abspath('main/temp')
        if  extension not in file_extension_accepted:
            return json.dumps({"status": 400, "code":cst.ERROR_CODE["FILE_NOT_OPEN"]})

        name = str(uuid.uuid1()) + '.'+ extension

        with open(os.path.join(path, name), "wb") as f:
            f.write(r.content)
        file_path = os.path.join(path, name)
        return file_path
    except Exception as e:
        log.info('An exception was raise when sending notification error: %s' % e)
        return json.dumps({"status": 400, "code": cst.ERROR_CODE["FILE_NOT_OPEN"]})


def valid_email(email):
        log.info(email)
        email = str(email)
        if '@' not in email:
            return False
        split_email = email.split('.')
        if len(split_email) <= 1 or len(split_email[-1]) < 2:
            return False
        return True
